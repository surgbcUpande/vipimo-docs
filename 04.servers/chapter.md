---
title: Servers
metadata:
    description: Documentation system for vipio projects
    author: Brian Onang'o
---

#### Chapter 4

# Servers

This is a comprehensive documentation for vipimo. Learn how to design, program and use vipimo hardware and all related software.

You can find information here about any of these:

- (Nodes)['/nodes']
- (Gateways)['/gateways']
- (Servers)['/servers']
- (APIs)['/apis']