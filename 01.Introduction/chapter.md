---
title: Introduction
metadata:
    description: Documentation system for vipio projects
    author: Brian Onang'o
---

#### Chapter 1

# Introduction

Learn how to design, program and use vipimo hardware and all related software.

This documentation is intended for internal use in [Upande Limited](https://upande.com). It's purpose it to bridge the gap between the hardware and software developers by providing a comprehensive reference material for each group to get as much knowledge as they need about hardware on the one hand and the related software on the other. It is also intended to be a starting point for any new person joining the organization. It might be that a knowledge of the technology discussed here may be required for interviews for joining Upande in the future.

You can find information here about any of these:

- [Nodes](/nodes)
- [Gateways](/gateways)
- [Servers](/servers)
- [APIs](/apis)