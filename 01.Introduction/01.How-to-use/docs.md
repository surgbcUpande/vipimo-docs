---
title: How to Use
metadata:
    description: Documentation system for vipio projects
    author: Brian Onang'o
---

#### How to Use

There are two ways in which you may want to use this documentation:
1. You may want to just use it as a reference. In which case the information in this section may not be of much use to you.
2. You may want to contribute to the content on this page. Remember that this system is built using [csycms](https://csycms.csymapp.com). Please check the documentation on how to use it [here](https://learn.csycms.csymapp.com). Then go clone [git@bitbucket.org:surgbcUpande/vipimo-docs.git](https://bitbucket.org/surgbcUpande/vipimo-docs/src/master/) and create your content.